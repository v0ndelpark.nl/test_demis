﻿<!DOCTYPE html>
<html lang="ru">

<head>

	<meta charset="utf-8">
	

	<title>Форма связи</title>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="css/style.css">
    
</head>

<body>
<div class="wrap">
<form method="post" action="" >

  <input type="text" id="name" name="name" placeholder="ФИО" required>
  
  <textarea id="address" name="address" placeholder="Адрес"></textarea>

  <input type="tel" id="phone" name="phone" onkeyup="this.value = this.value.replace(/[^\d]/g,'');" placeholder="Телефон" required>
  
  <input type="email" id="email" name="email" placeholder="Email" required> 

  <input type="submit" id="submit" value="Отправить">
</form>


<?php
  header("Content-Type: text/html; charset=utf-8");
  // Подключаемся к базе данных
  $db_host = 'localhost';
  $db_user = 'root';
  $db_pass = '';
  $db_name = 'database_test';
  $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

  // Проверяем, была ли отправлена форма
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Получаем данные из формы
    $name = mysqli_real_escape_string($conn, $_POST['name']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $phone = mysqli_real_escape_string($conn, $_POST['phone']);
    $address = mysqli_real_escape_string($conn, $_POST['address']);

    // Проверяем обязательные поля
    if (empty($name) || empty($email)) {
      die('Пожалуйста, заполните все обязательные поля.');
    }

    // Проверяем корректность данных
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      die('Пожалуйста, введите email адрес в верном формате.');
    }

    // Сохраняем данные в базу данных
    $sql = "INSERT INTO contacts (name, email, phone, address) VALUES ('$name', '$email', '$phone', '$address')";
    if (mysqli_query($conn, $sql)) {
      echo '';
    } else {
      echo 'Ошибка: ' . mysqli_error($conn);
    }
  }

  // Проверяем, была ли отправлена форма
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Получаем данные из базы данных
    $sql = "SELECT * FROM contacts";
    $result = mysqli_query($conn, $sql);

    // Выводим данные в виде таблицы
    echo '<table>';
    echo '<tr><th>Имя</th><th>Email</th><th>Телефон</th><th>Адрес</th></tr>';
    while ($row = mysqli_fetch_assoc($result)) {
      echo '<tr>';
      echo '<td>' . $row['name'] . '</td>';
      echo '<td>' . $row['email'] . '</td>';
      echo '<td>' . $row['phone'] . '</td>';
      echo '<td>' . $row['address'] . '</td>';
      echo '</tr>';
    }
    echo '</table>';

    // Закрываем соединение с базой данных
    mysqli_close($conn);
  }
  ?>


</div>



</body>
</html>
