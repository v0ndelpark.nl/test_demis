<!DOCTYPE html>
<html lang="ru">

<head>

	<meta charset="utf-8">
	

	<title>Новости</title>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="css/style.css">
    
</head>

<body>
<div class="container">
    <?php
      // Подключение к базе данных
  $db_host = 'localhost';
  $db_user = 'root';
  $db_pass = '';
  $db_name = 'database_test';
  $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }

      // Выборка новостей из базы данных
      $sql = "SELECT * FROM news";
      $result = $conn->query($sql);

      // Вывод новостей на страницу
      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          echo "<h2>" . $row["title"] . "</h2>";
          echo "<p>" . $row["content"] . "</p>";
          echo "<p><em>" . $row["date"] . "</em></p>";
        }
      } else {
        echo "Нет новостей для отображения.";
      }

      $conn->close();
    ?>
</div>
</body>