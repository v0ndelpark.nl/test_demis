<!DOCTYPE html>
<html lang="ru">

<head>

	<meta charset="utf-8">
	

	<title>Главная</title>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="css/style.css">
    
</head>

<body>
<div class="container">
  <h2>Последние новости</h2>
  <?php
  // подключаемся к БД
  $mysqli = new mysqli("localhost", "root", "", "database_test");
  
  // проверяем соединение
  if ($mysqli->connect_errno) {
    echo "Ошибка подключения к MySQL: " . $mysqli->connect_error;
    exit();
  }
  
  // выполняем SQL-запрос
  $result = $mysqli->query("SELECT title, LEFT(content, 100) AS preview, date FROM news ORDER BY date DESC LIMIT 3");
  
  // выводим результаты запроса
  while ($row = $result->fetch_assoc()) {
    echo "<div class='news-item'>";
    echo "<h3>" . $row["title"] . "</h3>";
    echo "<p>" . $row["preview"] . "...</p>";
    echo "<span class='date'>" . $row["date"] . "</span>";
    echo "</div>";
  }
  
  // закрываем соединение
  $mysqli->close();
  ?>
  <a href="news.php">Все новости</a><br>
  <a href="form.php">Обратная связь</a>
</div>

</body>